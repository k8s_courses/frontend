import React, { useState, useEffect } from 'react';

function App() {
  const [date, setDate] = useState('');

  useEffect(() => {
    fetch('/api')
      .then(res => res.json())
      .then(data => setDate(data[0].now))
      .catch(err => console.log(err));
  }, []);

  return (
    <div className="App">
      <h1>Current date and time:</h1>
      <p>{new Date(date).toLocaleString()}</p>
    </div>
  );
}

export default App;
